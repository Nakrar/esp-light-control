﻿﻿Mycropython Esp32 project

Allows:
control of led strips via GET requests to API (GET is chosen for ease of use)
real-time RGB-strip color control via web-sockets (which is in pair with Prismatic, creates immersive content-consumption experience)
animations
